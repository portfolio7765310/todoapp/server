const User = require('../models/userModel');
const bcrypt = require('bcrypt');
const validator = require('validator');
const jwt = require('jsonwebtoken');
const auth = require('../auth');


module.exports.registerUser = async (req, res) => {
	
	const {name, email, password} = req.body;

	let user = await User.findOne({email});

	if(user){
		return res.status(400).json('Email already exists');
	}
	if(!name || !email || !password){
		return res.status(400).json('All fields are required.')
	}
	if(!validator.isEmail(email)){
		return res.status(400).json('Email must be a valid email.');
	}
	if(!validator.isStrongPassword(password)){
		return res.status(400).json('Password must be a strong password.');
	}

	user = new User({name, email, password})

	const salt = await bcrypt.genSalt(10)
	user.password = await bcrypt.hash(user.password, salt)

	await user.save();

	const token = auth.createAccessToken(user);

	res.status(200).json({_id: user._id, name, email, token})
}


module.exports.loginUser = async (req, res) => {
	const { email, password } = req.body;

	
	let user = await User.findOne({email});

	if(!user){
		return res.status(400).json('Incorrect Email');
	}

	const isPasswordCorrect = await bcrypt.compare(password, user.password);

	if(!isPasswordCorrect){
		return res.status(400).json('Incorrect Password')
	}
	else{
		const token = auth.createAccessToken(user);

		res.status(200).json({_id: user._id, name: user.name, email, password, token})
	}

}

module.exports.getUsers = (req, res) =>{
	return User.find().then(result => {
		res.send (result);
	})
	.catch(error => {
		console.log(error);
		res.send(error);
	})
}



module.exports.findUserTask = async (req, res) => {
  const userId = req.params.userId;

  const user = await User.findById(userId);

  const taskIds = user.tasks.map((task) => task);

  res.status(200).json(taskIds);
}


module.exports.editUserTask = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  const userId = userData.id;

  const user = await User.findById(userId);

  const taskIds = user.tasks.map((task) => task._id);

  const taskId = req.params.taskId;

  res.status(200).json(taskId)
};
