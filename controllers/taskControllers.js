const mongoose = require('mongoose');
const Task = require('../models/taskModel');
const User = require('../models/userModel');
const bcrypt = require("bcrypt");
const auth = require("../auth");




module.exports.createTask = async(req, res) => {
	const {title, description} = req.body;
	const userData = auth.decode(req.headers.authorization)
	const userId = userData.id;

	let newTask = new Task({
		title: req.body.title,
		description: req.body.description,
		completed: false
	})

	const response = await newTask.save();

	
	let isUserUpdated = await User.findById(userId)
	.then(user => {
		user.tasks.push({
			title: response.title,
			description: response.description,
			completed: response.completed
		})

		return user.save()
		.then(result => {
			console.log(result);
			return true;
		})
		.catch(error => {
			console.log(error);
			return false;
		})
	})
	res.status(200).json(response);

}


module.exports.updateTask = async (req, res) => {
  const taskId = req.params.taskId;
  const userData = auth.decode(req.headers.authorization);
  const userId = userData.id;
  const { title, description } = req.body;

  const updatedUser = await User.findOneAndUpdate(
    { _id: userId, "tasks._id": taskId }, // find the user by id and the task by taskId
    { $set: { "tasks.$.title": title, "tasks.$.description": description } }, // update the task properties
    { new: true } // return the updated document
  );

  res.status(200).json(updatedUser);
}

module.exports.deleteTask = async (req, res) => {
  const taskId = req.params.taskId;
  const userData = auth.decode(req.headers.authorization);
  const userId = userData.id;

  const updatedUser = await User.findOneAndUpdate(
    { _id: userId, "tasks._id": taskId }, // find the user by id and the task by taskId
    { $pull: { tasks: { _id: taskId } } },
    { new: true } // return the updated document
  );

  res.status(200).json(updatedUser);
}
