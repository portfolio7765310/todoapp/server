const mongoose = require('mongoose');

const taskSchema = new mongoose.Schema({
  title: {
    type: String,
    required: [true, 'Task name is required.']
  },
  description: {
    type: String,
    required: [true, 'Task description is required'],
  },
  completed: {
    type: Boolean,
    default: false,
  }
},
{
	timestamps: true
}
);

module.exports = mongoose.model('Task', taskSchema);