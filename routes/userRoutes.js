const express = require('express');
const userController = require('../controllers/userControllers');
const auth = require('../auth');

const router = express.Router();

router.post('/register', userController.registerUser);

router.post('/login', userController.loginUser);

router.get('/getUsers', userController.getUsers);

router.get('/find/:userId', userController.findUserTask);

router.patch('/edit/:taskId', auth.verify, userController.editUserTask);





module.exports = router;