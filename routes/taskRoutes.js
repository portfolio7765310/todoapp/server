const express = require('express');
const taskController = require('../controllers/taskControllers');
const auth = require('../auth');

const router = express.Router();

router.post('/create', auth.verify, taskController.createTask);


router.patch('/edit/:taskId', taskController.updateTask);

router.delete('/delete/:taskId', taskController.deleteTask);



module.exports = router;