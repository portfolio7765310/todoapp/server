const express = require('express');
const cors = require('cors');
const mongoose = require ('mongoose');
const bodyParser = require('body-parser')

const userRoute = require('./routes/userRoutes');
const taskRoute = require('./routes/taskRoutes');

const http = require('http');
const app = express();
const server = http.createServer(app);


// Middleware
app.use(express.json());
app.use(cors());
app.use(express.urlencoded({extended: true}));
app.use('/users', userRoute);
app.use('/tasks', taskRoute);
app.use(bodyParser.json())



app.listen(process.env.PORT || 5001, () => {
	console.log(`API is now online on port ${process.env.PORT || 5001}`)
})

mongoose.connect("mongodb+srv://admin:admin@todo-app.vznffoc.mongodb.net/todoApp?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once("open", () => console.log("Now connected to Magcale-Mongo DB Atlas"));